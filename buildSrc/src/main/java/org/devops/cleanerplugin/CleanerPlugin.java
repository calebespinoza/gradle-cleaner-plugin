package org.devops.cleanerplugin;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class CleanerPlugin implements Plugin<Project> {
    public void apply(Project project) {
        project.getTasks().create("cleaner-plugin", Cleaner.class);
    }
}
