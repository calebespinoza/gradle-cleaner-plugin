package org.devops.cleanerplugin;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import java.io.File;

public class Cleaner extends DefaultTask {
    @TaskAction
    void cleaner() {
        String target1 = "build";
        String target2 = "buildSrc";

        File index = new File(System.getProperty("user.dir"));
        ;
        for (String item : index.list()) {
            if(item.matches(target1)){
                File files = new File(index + "/" + item);
                deleteDir(files);
                System.out.println("Directory: " + item + " has been deleted!");
            } else if (item.matches(target2)) {
                File files = new File(index + "/" + item + "/build");
                deleteDir(files);
                System.out.println("Directory: " + item + "/build" + " has been deleted!");
            }
        }
        //deleteDir(files);
    }

    void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }
}
